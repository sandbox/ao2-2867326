(function ($) {
  // Populate the placeholder element.
  $('div.node-toggle-content-link.uncollapsed').html('<button type="button">[-]</button>');
  $('div.node-toggle-content-link.collapsed').html('<button type="button">[+]</button>');

  // Initialize the visibility status of collapsed elements.
  $('div.node-toggle-content-link.collapsed').closest('.node-toggle-content-region').find('.node__meta').hide();
  $('div.node-toggle-content-link.collapsed').closest('.node-toggle-content-region').find('.node__content .field--name-body').hide();

  // Toggle visibility of the node content.
  $('div.node-toggle-content-link button').click(function(event) {
    $(this).closest('.node-toggle-content-region').find('.node__meta').toggle();
    $(this).closest('.node-toggle-content-region').find('.node__content .field--name-body').toggle();

    if ($(this).text() == '[+]') {
      $(this).text('[-]');
      $(this).parent().removeClass('collapsed').addClass('uncollapsed');
    }
    else if ($(this).text() == '[-]') {
      $(this).text('[+]');
      $(this).parent().removeClass('uncollapsed').addClass('collapsed');
    }
  });
})(jQuery);
