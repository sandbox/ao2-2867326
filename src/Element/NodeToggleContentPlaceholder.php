<?php

namespace Drupal\node_toggle_content\Element;

use Drupal\Core\Template\Attribute;
use Drupal\Core\Render\Element\RenderElement;
use Drupal\Component\Utility\SafeMarkup;

/**
 * Provides a node_toggle_content_placeholder element.
 *
 * @RenderElement("node_toggle_content_placeholder")
 */
class NodeToggleContentPlaceholder extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#pre_render' => [
        [$class, 'preRenderPlaceholder'],
      ],
    ];
  }

  /**
   * Pre-render callback: Renders a content toggle placeholder into #markup.
   *
   * Renders an empty (hence invisible) placeholder div attributes (namely,
   * class), which allows the JavaScript code to select the element and
   * dynamically render a toggle button.
   *
   * @param array $element
   *   A structured array with #attributes containing a "class" selector.
   *
   * @return array
   *   The passed-in element with the content toggle placeholder in '#markup'.
   */
  public static function preRenderPlaceholder(array $element) {
    $element['#markup'] = SafeMarkup::format('<div@attributes></div>', ['@attributes' => new Attribute($element['#attributes'])]);

    return $element;
  }

}
